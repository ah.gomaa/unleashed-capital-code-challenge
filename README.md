## Description

Graphql API for managing the simple bank accounts.

## Installation

```bash
$ npm install
```

## Running the app (With testing)

```bash
# Database & API
$ docker-compose up -d

# Run the db migration
$ npm run db:migrate

# Seed the accounts table
$ npm run db:seed

# Get account balance by id
$ curl 'http://localhost:3000/graphql' -H 'Content-Type: application/json' --data-binary '{"query":"{\n  account(id: \"37668d2d-2f45-4daf-ab21-5977dac3fe80\"){\n    balance\n  }\n}"}' --compressed

# Transfer balance from one account to another
$ curl 'http://localhost:3000/graphql' -H 'Content-Type: application/json' --data-binary '{"query":"mutation {\n  balanceTransfer(\n    recipientAccountId: \"37668d2d-2f45-4daf-ab21-5977dac3fe80\", \n    senderAccountId:\"37668d2d-2f45-4daf-ab21-5977dac3fe81\",\n    amount: 10\n  ) {\n    transactionStatus\n    senderCurrentBalance\n  }\n}"}' --compressed
```

## Running the API

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod

# docker-compose
$ docker-compose up -d api
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests (no e2e tests were added)
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

# Database

## Start local db server
```
docker-conpose up -d db
```

## Create New Migration
```
npm run db:migrate:make $MIGRATION_NAME
```

## Run Latest Migration
```
npm run db:migrate
```

## Rollback Latest Migration
```
npm run db:rollback
```

## Seed the db
```
npm run db:seed
```