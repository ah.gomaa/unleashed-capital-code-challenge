// This is only for the purpose of testing the task, could be optimized for better and wider uses
import { dbClient } from "../src/api/common/utils/dbClient"
import { DBAccount } from "../src/api/datasources/accounts.store.types";
import { ACCOUNTS_TABLE_NAME } from "../src/config";

const accounts: DBAccount[] = [
  { account_id: '37668d2d-2f45-4daf-ab21-5977dac3fe80', balance: 100, updated_at: new Date().toISOString() },
  { account_id: '37668d2d-2f45-4daf-ab21-5977dac3fe81', balance: 100, updated_at: new Date().toISOString() }
]

const seedAccountsTable = async () => {
  console.log(`Inserting ${accounts.length} entries to the accounts table`)

  const rows = await dbClient.batchInsert(ACCOUNTS_TABLE_NAME, accounts, 20).returning('account_id');

  console.log(`Inserting done successfully`, rows)
}

seedAccountsTable()