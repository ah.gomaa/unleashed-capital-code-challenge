import { ACCOUNTS_TABLE_NAME } from '../../config';
import { Account } from '../models/account.model';
import { dbClient } from '../common/utils/dbClient';
import { AccountBalanceOperation, DBAccount, IAccountsStore, ITransaction } from './accounts.store.types';
import { Injectable } from '@nestjs/common';
import { Knex } from 'knex';

@Injectable()
export class AccountsStore implements IAccountsStore {
  async findAccountById(id: string): Promise<Account> {
    const [account] = await dbClient.select<DBAccount[]>(`*`).from(ACCOUNTS_TABLE_NAME).where('account_id', '=', id);

    if (!account) return;

    return this.mapDbAccountToAccountModel(account);
  }

  // TODO: Add unit tests for this method
  async updateAccountsBalance(
    operations: AccountBalanceOperation[],
    transaction?: Knex.Transaction,
  ): Promise<Account[]> {
    const client = transaction || dbClient;

    // TODO: Find a better way to update in one go instead of multiple requests
    const results = await Promise.all(
      operations.map((operation) =>
        client.raw(`
            UPDATE ${ACCOUNTS_TABLE_NAME}
            SET balance = balance ${operation.operator} ${operation.amount}
            WHERE account_id = '${operation.accountId}'
            returning *
          `),
      ),
    );

    return results.flatMap((result) =>
      (result.rows as DBAccount[])?.map((row) => this.mapDbAccountToAccountModel(row)),
    );
  }

  startTransaction(): Promise<ITransaction> {
    return dbClient.transaction();
  }

  private mapDbAccountToAccountModel(dbAccount: DBAccount) {
    return {
      id: dbAccount.account_id,
      balance: dbAccount.balance,
    };
  }
}
