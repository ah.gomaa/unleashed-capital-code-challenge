import { Knex } from 'knex';
import { Account } from '../models/account.model';

export interface AccountBalanceOperation {
  accountId: string;
  operator: '+' | '-';
  amount: number;
}

// TODO: Look into better abstraction as commit & rollback still hold all the Knex properties
export type ITransaction = Pick<Knex.Transaction, 'commit' | 'rollback'>;

export abstract class IAccountsStore {
  abstract findAccountById(id: string, transaction?: ITransaction): Promise<Account>;
  abstract updateAccountsBalance(operations: AccountBalanceOperation[], transaction?: ITransaction): Promise<Account[]>;
  abstract startTransaction(): Promise<ITransaction>;
}

export interface DBAccount {
  account_id: string;
  balance: number;
  updated_at: string;
}
