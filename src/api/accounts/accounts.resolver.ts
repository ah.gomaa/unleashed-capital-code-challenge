import { NotFoundException, BadRequestException } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Account } from '../models/account.model';
import { BalanceTransferTransaction } from '../models/transaction.model';
import { AccountsService } from './accounts.service';

@Resolver(() => Account)
export class AccountsResolver {
  constructor(private readonly accountsService: AccountsService) {}

  @Query(() => Account)
  async account(@Args('id') id: string): Promise<Account> {
    if (!id) {
      throw new BadRequestException('id param is missing');
    }

    const account = await this.accountsService.findOneById(id);

    if (!account) {
      throw new NotFoundException(id);
    }

    return account;
  }

  @Mutation(() => BalanceTransferTransaction)
  async balanceTransfer(
    @Args('senderAccountId') senderAccountId: string,
    @Args('recipientAccountId') recipientAccountId: string,
    @Args('amount') amount: number,
  ): Promise<BalanceTransferTransaction> {
    const errors: string[] = [];

    if (!senderAccountId) {
      errors.push('Missing senderAccountId param');
    }

    if (!recipientAccountId) {
      errors.push('Missing recipientAccountId param');
    }

    if (!amount || amount <= 0) {
      errors.push('amount can not be missing, equals to 0 or less than 0');
    }

    if (errors.length) {
      throw new BadRequestException(errors.join(', '));
    }

    const result = await this.accountsService.balanceTransfer({
      senderId: senderAccountId,
      recipientId: recipientAccountId,
      amount,
    });

    return {
      transactionStatus: result.transactionStatus,
      message: result.message,
      senderCurrentBalance: result.senderAccount?.balance,
    };
  }
}
