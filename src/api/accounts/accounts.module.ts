import { Module } from '@nestjs/common';
import { AccountsStore } from '../datasources/accounts.store';
import { IAccountsStore } from '../datasources/accounts.store.types';
import { AccountsResolver } from './accounts.resolver';
import { AccountsService } from './accounts.service';

@Module({
  providers: [
    AccountsResolver,
    AccountsService,
    {
      provide: IAccountsStore,
      useClass: AccountsStore,
    },
  ],
})
export class AccountsModule {}
