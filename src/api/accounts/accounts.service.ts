import { Injectable, NotFoundException } from '@nestjs/common';
import { IAccountsStore } from '../datasources/accounts.store.types';
import { Account } from '../models/account.model';
import { TransactionStatus } from '../models/transaction.model';

@Injectable()
export class AccountsService {
  constructor(private readonly accountsStore: IAccountsStore) {}

  async findOneById(id: string): Promise<Account> {
    return this.accountsStore.findAccountById(id);
  }

  async balanceTransfer({
    senderId,
    recipientId,
    amount,
  }: {
    senderId: string;
    recipientId: string;
    amount: number;
  }): Promise<{
    transactionStatus: TransactionStatus;
    message: string;
    senderAccount?: Account;
  }> {
    const transaction = await this.accountsStore.startTransaction();

    const updatedAccounts = await this.accountsStore.updateAccountsBalance(
      [
        { accountId: senderId, operator: '-', amount },
        { accountId: recipientId, operator: '+', amount },
      ],
      transaction,
    );

    let sender: Account, recipient: Account;
    updatedAccounts?.forEach((account) => {
      if (account.id === senderId) {
        sender = account;
      }
      if (account.id === recipientId) {
        recipient = account;
      }
    });

    if (!sender) {
      transaction.rollback();
      throw new NotFoundException('The sender account is not found');
    }

    if (!recipient) {
      transaction.rollback();
      throw new NotFoundException('The recipient account is not found');
    }

    if (sender?.balance < 0) {
      await transaction.rollback();
      return {
        transactionStatus: TransactionStatus.FAILED,
        message: 'Sender balance is not enough',
        // Get the actual balance as the sender variable holds the balance from the transaction
        senderAccount: await this.accountsStore.findAccountById(senderId),
      };
    }

    await transaction.commit();

    return {
      transactionStatus: TransactionStatus.SUCCESSFUL,
      message: 'Transaction completed successfully',
      senderAccount: sender,
    };
  }
}
