import knex from 'knex';
import { DATABASE_URL } from '../../../config';

export const dbClient = knex({
  client: 'pg',
  connection: DATABASE_URL,
});
