import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'account' })
export class Account {
  @Field(() => ID)
  id: string;

  @Field()
  balance: number;
}
