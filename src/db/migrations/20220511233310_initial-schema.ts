import { Knex } from 'knex';
import { ACCOUNTS_TABLE_NAME } from '../../config';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(ACCOUNTS_TABLE_NAME, (table) => {
    table.uuid('account_id').primary();
    table.decimal('balance').notNullable();
    table.timestamp('updated_at');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(ACCOUNTS_TABLE_NAME);
}
