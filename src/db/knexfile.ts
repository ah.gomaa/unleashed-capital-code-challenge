import { DATABASE_URL } from '../config';

export default {
  client: 'pg',
  connection: DATABASE_URL,
  migrations: {
    extension: 'ts',
  },
};
