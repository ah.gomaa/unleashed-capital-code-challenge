// General
export const PORT = process.env.PORT || 3000;

// Database
export const DATABASE_URL = process.env.DATABASE_URL || 'postgres://postgres@localhost:5432/unleashedCapital';
export const ACCOUNTS_TABLE_NAME = process.env.ACCOUNTS_TABLE_NAME || 'accounts';
